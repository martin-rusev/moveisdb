package com.martin.movies.utils;

public class UrlParser {

    private static final String OMDB_LINK = "http://www.omdbapi.com/?apikey=";

    /**
     *  The UrlParser's only static method accepts an imdb movie identifier and
     *  parses the movie url with the apikey
     */
    public static String parseUrl(String imdbMovieId) {
        StringBuilder sb = new StringBuilder();
        sb.append(OMDB_LINK);
        sb.append(System.getenv("OMDB_APIKEY"));
        sb.append("&i=");
        sb.append(imdbMovieId);
        return sb.toString();
    }
}

